import simplejson as json
import os
import shutil
import datetime

import urllib
import sys

from main.models import Experiment, GeneGO,ArraySpot,Transcript,Marker, Species, Chromosome, GeneInfo
from math import factorial
from django.conf import settings

import numpy as np
import pandas as pd

def testGOaccession(exp_name,genelist,GO_accession):
    GI = GeneInfo.objects.filter(arrayspot__experiment__experiment_name = exp_name)
    ctotalGenes = GI.count()

    GIGO = GI.filter(genego__term_accession = GO_accession)
    ctotalGenesAccession = GIGO.count()
    cgenelistAccession = GIGO.filter(gene_id__in = genelist).count()
    cgenelist = len(genelist)
    N = ctotalGenes 
    m1 = 1
    m2 = N - m1
    n1 = 1
    n2 = 1
    a = 1
    b = 1
    c = 1
    d = 1
    pvalue = factorial(m1)*factorial(m2)*factorial(n1)*factorial(n2)/(factorial(N)*factorial(a)*factorial(b)*(factorial(c)*(factorial(d))))
    return pvalue


def loadGO_OBO(filename): 
    with open(filename) as GO_OBO_File:
        for line in GO_OBO_File:
            pass


def load_genelist_for_experiment(experiment_name):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    genes_pickle_path = os.path.join(experiment_data_path, '%s' % "genes.npy")
    genes = np.load(genes_pickle_path)
    return genes


def load_markerlist_for_experiment(experiment_name):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    markers_pickle_path = os.path.join(experiment_data_path, '%s' % "markers.npy")
    markers = np.load(markers_pickle_path)
    return markers


def load_lodscores_for_experiment(experiment_name):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    lod_pickle_path = os.path.join(experiment_data_path, '%s' % "lod.npy")
    lod_scores = np.load(lod_pickle_path)
    return lod_scores


def create_pickles_for_experiment(experiment_name):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    lod_pickle_path = os.path.join(experiment_data_path, '%s' % "lod.npy")
    genes_pickle_path = os.path.join(experiment_data_path, '%s' % "genes.npy")
    markers_pickle_path = os.path.join(experiment_data_path, '%s' % "markers.npy")

    if not os.path.exists(lod_pickle_path) or \
            not os.path.exists(genes_pickle_path) or \
            not os.path.exists(markers_pickle_path):
        lod_file = Experiment.objects.get(experiment_name=experiment_name).lod_file
        lod_file_path = os.path.join(experiment_data_path, '%s' % lod_file)

        with open(lod_file_path) as lodfile:
            m = lodfile.readline().split()[1:]
            ncol = len(m)
            g = []
            l = []
            nrow = 0
            for line in lodfile:
                fields = line.split()
                g.append(fields[0].upper())
                l.append(fields[1:])
                nrow += 1

        markers = np.asarray(m)
        genes = np.asarray(g)
        a = np.asarray(l)
        data = a.reshape(nrow, ncol).astype(np.float)
        np.save(markers_pickle_path, markers)
        np.save(lod_pickle_path, data)
        np.save(genes_pickle_path, genes)


def update_max_lod_in_database():
    for experiment_name in Experiment.objects.filter(species__species_name=settings.SPECIES).values_list("experiment_name",flat=True):

        genes = load_genelist_for_experiment(experiment_name)
        lod_scores = load_lodscores_for_experiment(experiment_name)
        max_lod_gene = np.amax(np.fabs(lod_scores),axis=1)
        gene2max =dict(zip(genes,max_lod_gene))

        genes_for_experiment = ArraySpot.objects.filter(experiment__experiment_name=experiment_name)
        for gene in genes_for_experiment:
            if gene.spot_id in gene2max:
                gene.max_lod_score = gene2max[gene.spot_id]
            else:
                gene.max_lod_score = 0
            gene.save()


def create_pickles():
    for experiment_name in Experiment.objects.filter(species__species_name=settings.SPECIES).values_list("experiment_name",flat=True):
        create_pickles_for_experiment(experiment_name)


def getChrMarkers(experiment_name, chromosome):
    if len(Chromosome.objects.filter(name=chromosome,species__species_name=settings.SPECIES)) == 0:
        try:
            chromosome = int(chromosome)
            if chromosome > 0 and chromosome < 10:
                roman = ["I","II","III","IV","V","VI","VII","VIII","IX"]
                chromosome = roman[chromosome - 1]
        except ValueError:
            pass
    return Marker.objects.filter(experiment__experiment_name = experiment_name, chromosome__name = chromosome)

def getClosestMarker(experiment_name, chromosome, pos):
    pos = int(pos)
    markers = getChrMarkers(experiment_name, chromosome)
    minDist = -1
    closestMarker = None
    for marker in markers:
        dist = abs(pos - marker.start)
        if minDist == -1 or dist < minDist:
            minDist = dist
            closestMarker = marker

    return closestMarker


def getMarkerObjects(experiment_name):
    return Marker.objects.filter(experiment__experiment_name = experiment_name)


def getSpeciesForExperiment(experiment_name):
    return Species.objects.get(experiment__experiment_name = experiment_name).species_name


def getMarkerNames(experiment_name):
    return Marker.objects.filter(experiment__experiment_name = experiment_name).values_list("name", flat=True)


def createSettingsJsonForExperiment(experiment_name):
    
    output_dic = dict()

    species_name = getSpeciesForExperiment(experiment_name)
    output_dic['species'] = species_name

    url = Species.objects.filter(species_name=species_name).values_list('url', flat=True)
    if len(url) == 1:
        output_dic['url'] = url[0]
    else:
        output_dic['url'] = ""

    # only report chromosomes for with markers are
    chromosomes = Chromosome.objects.filter(marker__experiment__experiment_name=experiment_name).distinct()
    output_dic['chrnames'] = sorted(chromosomes.values_list('name', flat=True))

    chr_dic = dict()
    for chromosome in chromosomes:
        chr_dic[chromosome.name] = {"start": chromosome.start, "end": chromosome.end}

    output_dic["chr"] = chr_dic

    out_file = os.path.join(settings.MEDIA_ROOT,'data/%s/%s.json' % (experiment_name,"settings"))

    with open(out_file, 'w') as fo:
        json.dump(output_dic, fo, indent=4, ensure_ascii=True)

def createGeneJsonForExperiment(experiment_name):
    '''
    description: parse upload information into JSON file format later which later will be used for multiexperimentplot plot. 

    @type experiment_name: name of experiment
    @param experiment_name: string
    
    @type thld: LOD theshold
    @param thld: decimal

    @rtype output: path of output file
    @return output: string
    
    '''

    experiment = Experiment.objects.get(experiment_name=experiment_name)
    qtl_file = experiment.lod_file
    in_path = os.path.join(settings.MEDIA_ROOT,'data/%s/%s' % (experiment_name,qtl_file))

    species_name = getSpeciesForExperiment(experiment_name)
    geneInfo_list = GeneInfo.objects.filter(species__species_name=species_name)
    geneInfo_dict = dict((geneInfo.gene_id, geneInfo) for geneInfo in geneInfo_list)

    marker_list = Marker.objects.filter(experiment__experiment_name=experiment_name)
    marker_dict = dict((marker.name, marker) for marker in marker_list)

    phenotypes_file = os.path.join(settings.MEDIA_ROOT,'data/%s/%s' % (experiment_name,"phenotypes.txt"))
    if os.path.exists(phenotypes_file):
        phenotypes = pd.read_csv(phenotypes_file, sep='\t')
        phenotypes.index = map(str.upper, phenotypes.index)
    else:
        phenotypes = None

    genes_directory = os.path.join(settings.MEDIA_ROOT, 'data/%s/genes/' % (experiment_name))
    if not os.path.exists(genes_directory):
        print "making %s"%(genes_directory)
        os.makedirs(genes_directory)

    probes_directory = os.path.join(settings.MEDIA_ROOT, 'data/%s/probe/' % (experiment_name))
    if not os.path.exists(probes_directory):
        print "making %s"%(probes_directory)
        os.makedirs(probes_directory)

    with open(in_path) as fi:
        first_line = fi.readline().rstrip()
        markers = first_line.split('\t')[1:]
        for markerName in markers:
            if markerName not in marker_dict:
                sys.stderr.write("marker not in database: %s\n" % markerName)
                return()

        for line in fi:
            output_dic = {}
            fields = line.split('\t')
            gene = fields[0].upper()

            output_dic["gene"] = gene

            if gene not in geneInfo_dict:
                sys.stderr.write("gene_id not in database: %s, (species_name: %s)\n"%(gene, species_name))
                continue
            geneInfo = geneInfo_dict[gene]
            output_dic["name"] = geneInfo.gene_name
            output_dic["chr"] = geneInfo.chr
            output_dic["start"] = geneInfo.start
            output_dic["end"] = geneInfo.end
            output_dic["experiment"] = experiment_name
            lodscore_dict = {}

            for idx, markerName in enumerate(markers):
                marker = marker_dict[markerName]
                lodscore_dict[markerName] = dict(chr=marker.chromosome.name, start=marker.start, end=marker.end,
                                               lod=fields[idx + 1])

            output_dic["lodscores"] = lodscore_dict

            out_file = os.path.join(settings.MEDIA_ROOT, 'data/%s/genes/%s.json' % (experiment_name, gene))

            with open(out_file, 'w') as fo:
                json.dump(output_dic, fo, indent=4)

            probe_output_dic = dict()
            probe_output_dic["probe"] = gene
            probe_output_dic["lod"] = list(map(float, fields[1:]))
            if phenotypes is not None:
                probe_output_dic["pheno"] = list(phenotypes.loc[gene])
            probe_out_file = os.path.join(settings.MEDIA_ROOT, 'data/%s/probe/%s.json' % (experiment_name, gene))

            with open(probe_out_file, 'w') as fo:
                json.dump(probe_output_dic, fo, indent=4)


def check_for_new_experiment():
    data_path = os.path.join(settings.MEDIA_ROOT, 'data/')
    experiment_dirs = os.listdir(data_path)
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES).values_list("experiment_name", flat=True)
    for experiment_dir in experiment_dirs:
        if experiment_dir[0] == ".":
            continue
        if experiment_dir not in experiments:
            create_experiment(experiment_dir)


def load_marker_file_in_database(experiment_name):
    Marker.objects.filter(experiment__experiment_name=experiment_name).delete()
    experiment = Experiment.objects.get(experiment_name=experiment_name)
    marker_file_name = experiment.marker_file
    marker_file_path = os.path.join(settings.MEDIA_ROOT, 'data/%s/%s' % (experiment_name,marker_file_name))
    with open(marker_file_path) as marker_file:
        header = marker_file.readline()
        for line in marker_file:
            fields = line.rstrip().split()
            marker = Marker()
            marker.name = fields[0]
            marker.start = "%d"%int(float(fields[2]))
            marker.end = "%d"%int(float(fields[3]))
            chromosome = Chromosome.objects.get(name=fields[1],species__species_name=settings.SPECIES)
            marker.chromosome = chromosome
            marker.experiment = experiment
            try:
                marker.save()
            except ValueError as e:
                sys.stderr.write("cannot import marker line: %s (%s)")%(line,e.strerror)


def add_genes_for_experiment(experiment_name):
    ArraySpot.objects.filter(experiment__experiment_name = experiment_name).delete()
    experiment = Experiment.objects.get(experiment_name=experiment_name)
    species = Species.objects.get(species_name=settings.SPECIES)

    lod_file_name = experiment.lod_file
    lod_file_path = os.path.join(settings.MEDIA_ROOT, 'data/%s/%s' % (experiment_name, lod_file_name))
    with open(lod_file_path) as lod_file:
        header = lod_file.readline()
        for line in lod_file:
            fields = line.rstrip().split()
            gene = fields[0].upper()

            gis = GeneInfo.objects.filter(gene_id=gene)
            if len(gis) == 0:
                gi = GeneInfo()
                gi.gene_info = gene
                gi.species = species
                gi.save()

            gene_info = GeneInfo.objects.get(gene_id=gene)

            arrayspots = ArraySpot.objects.filter(experiment__experiment_name = experiment_name).filter(spot_id = gene)
            if len(arrayspots) == 0:
                arrayspot = ArraySpot()
                arrayspot.experiment = experiment
                arrayspot.spot_id = gene
                arrayspot.geneinfo = gene_info
                arrayspot.save()

def create_experiment(experiment_name):
    experiment_dir = os.path.join(settings.MEDIA_ROOT, 'data/%s/' % (experiment_name))
    if not os.path.exists(experiment_dir):
        sys.stderr.write("Cannot create experiment, directory does not exist: %s\n" % experiment_dir)
        return

    species = Species.objects.get(species_name=settings.SPECIES)
    Experiment.objects.filter(experiment_name=experiment_name).delete()
    experiment = Experiment()
    experiment.experiment_name = experiment_name
    experiment.species = species
    experiment.marker_file = "marker.txt"
    experiment.array_file = "array.txt"
    experiment.lod_file = "lod.txt"
    experiment.pub_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    experiment.upload_user_id = 1
    experiment.pvalthld = 0.01
    experiment.lodthld = 3
    experiment.save()

    load_marker_file_in_database(experiment_name)
    add_genes_for_experiment(experiment_name)
    createSettingsJsonForExperiment(experiment_name)
    createGeneJsonForExperiment(experiment_name)


def rename_experiment(old_name,new_name):
    old_experiment_dir = os.path.join(settings.MEDIA_ROOT, 'data/%s/' % (old_name))
    if not os.path.exists(old_experiment_dir):
        sys.stderr.write("Cannot rename experiment, directory does not exist: %s\n"%old_experiment_dir)
        return

    new_experiment_dir = os.path.join(settings.MEDIA_ROOT, 'data/%s/' % (new_name))
    if os.path.exists(new_experiment_dir):
        sys.stderr.write("Cannot rename experiment, new directory already exists: %s\n"%new_experiment_dir)
        return

    shutil.move(old_experiment_dir,new_experiment_dir)

    experiment = Experiment.objects.get(experiment_name=old_name)
    experiment.experiment_name = new_name
    experiment.save()

    createGeneJsonForExperiment(new_name)


