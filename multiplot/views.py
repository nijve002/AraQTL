from django.shortcuts import render_to_response, HttpResponse, redirect
from django.conf import settings

from main.models import Experiment, GeneInfo, ArraySpot, GeneGO, GO, Species

import suggestions
import main

import urllib2
import re

class GeneInfoMultiplot:
    description = ""
    gene_name = ""
    chr = 0
    start = 0
    max_lod_score = 0

# Create your views here.

def multiplot(request):
    '''
    select a gene and experiment
    
    '''
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)
    species_short_name = Species.objects.get(species_name=settings.SPECIES).short_name

    if request.method == 'GET':
        if 'query' in request.GET:
            if request.GET.get('experiment_name'):
                exp_name = request.GET.get('experiment_name')
            else:
                exp_name = "all"
            query = request.GET.get('query')
            query = urllib2.unquote(query).strip()
            query = re.sub(r'\s+', r' ', query)

            if not query:
                return main.views.no_results(query, "multiplot")

            if exp_name == "all":
                if query.startswith("GO"):
                    exp_name = experiments[0].experiment_name
                else:
                    return multiexperimentplot(request)

            title = ""

            if re.match("GO:", query, re.I):
                genelist = queryGO(exp_name, query)
                if len(genelist) > 0:
                    title = "%s (%s)" % (GO.objects.filter(accession=query).values_list("name", flat=True)[0], query)
            else:
                genelist = queryGenesForExperiment(exp_name, query)
                if len(genelist) == 0:
                    ret = suggestions.views.index(request)
                    if ret:
                        return ret
                    else:
                        return main.views.no_results(query,"multiplot")
                elif len(genelist) == 1:
                    geneInfoList = GeneInfo.objects.filter(gene_id=query)
                    title = "%s" % query

                    if (geneInfoList.count() ==  1):
                        gene_name = geneInfoList[0].gene_name
                        if gene_name:
                            title += " (%s)" % gene_name


            genelist = set(genelist)
            geneInfoList = getGeneInfoForGeneIDs(genelist,exp_name)
            if len(geneInfoList) > 1:
                title += " %s genes"%len(geneInfoList)

            return render_to_response('multiplot.html', {'experiment_name': exp_name,
                                                         'experiments': experiments,
                                                         'query': query,
                                                         'species': species_short_name,
                                                         'title': title,
                                                         'gene_info_list': sorted(geneInfoList,reverse=True,
                                                                                  key=lambda x: x.max_lod_score),
                                                         })
        else:
            return render_to_response('multiplot.html', {'experiments': experiments})


def multiexperimentplot(request):
    '''
    plot lod scores for one gene for all experiments
    
    '''
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)
    species_short_name = Species.objects.get(species_name=settings.SPECIES).short_name

    query = request.GET.get('query')
    query = urllib2.unquote(query).strip().upper()

    genes = ArraySpot.objects.filter(geneinfo__gene_id=query)

    if genes.count() == 0:
        gi = GeneInfo.objects.filter(gene_name__iexact=query)

        if gi.count() == 0:
            gi = GeneInfo.objects.filter(description__icontains=query)

        if gi.count() == 1:
            query = gi[0].gene_id
        else:
            ret = suggestions.views.index(request)
            if ret:
                return ret
            else:
                return main.views.no_results(query,"multiplot")

    experiments4gene = Experiment.objects.filter(arrayspot__spot_id=query).values_list('experiment_name', flat=True)

    geneInfoList = GeneInfo.objects.filter(gene_id=query, species__species_name=settings.SPECIES)
    if (geneInfoList.count() == 0):
        return HttpResponse('<h1> Unknown gene %s </h1>' % query)
    gene = geneInfoList[0]

    return render_to_response('multiplot.html', {'gene': gene,
                                                 'experiments': experiments,
                                                 'query': query,
                                                 'experiment_name': "all",
                                                 'experiments4gene': experiments4gene,
                                                 'species': species_short_name})


def getGeneInfoForGeneIDs(geneIDs,experiment_name):
    geneInfoList = list()
    for geneID in geneIDs:
        gis = GeneInfo.objects.filter(gene_id=geneID)
        if len(gis) == 1:
            gi = gis[0]
            gim = GeneInfoMultiplot()
            gim.gene_name = gi.gene_name
            gim.gene_id = gi.gene_id
            gim.description = gi.description
            gim.chr = gi.chr
            gim.start = gi.start
            gim.max_lod_score = ArraySpot.objects.get(spot_id = geneID, experiment__experiment_name=experiment_name).max_lod_score
            geneInfoList.append(gim)

    return geneInfoList


def queryGenesForExperiment(exp_name, query):
    geneIDs = query.split(" ")

    return ArraySpot.objects.filter(experiment__experiment_name=exp_name).filter(spot_id__in=geneIDs).values_list(
        'spot_id', flat=True)


def queryGO(exp_name, query):
    GO_ID = query

    genelist = list(GeneGO.objects.filter(term_accession=GO_ID).values_list('geneinfo__gene_id', flat=True))

    genelist = set(genelist)

    genelist = queryGenesForExperiment(exp_name, ' '.join(genelist))
    return genelist
