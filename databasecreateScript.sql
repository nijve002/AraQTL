CREATE DATABASE `PlantQTL` /*!40100 DEFAULT CHARACTER SET latin1 */;
CREATE TABLE `main_arrayspot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spot_id` varchar(50) NOT NULL,
  `experiment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_main_arrayspot_experiment_idx` (`experiment_id`),
  CONSTRAINT `fk_main_arrayspot_experiment` FOREIGN KEY (`experiment_id`) REFERENCES `main_experiment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=241654 DEFAULT CHARSET=utf8;
CREATE TABLE `main_arrayspot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spot_id` varchar(50) NOT NULL,
  `experiment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_main_arrayspot_experiment_idax` (`experiment_id`),
  CONSTRAINT `fk_main_arrayspot_experiment` FOREIGN KEY (`experiment_id`) REFERENCES `main_experiment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=241654 DEFAULT CHARSET=utf8;

CREATE TABLE `main_experiment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `species_id` int(11) NOT NULL,
  `experiment_name` varchar(50) NOT NULL,
  `phenotypes` varchar(50) NOT NULL,
  `type_of_array` varchar(50) NOT NULL,
  `sample_size` varchar(50) NOT NULL,
  `parental_strain` varchar(50) NOT NULL,
  `reference` varchar(200) NOT NULL,
  `pubmed` varchar(20) NOT NULL,
  `pub_date` datetime NOT NULL,
  `upload_user_id` int(11) NOT NULL,
  `array_file` varchar(100) NOT NULL,
  `marker_file` varchar(100) NOT NULL,
  `genotype_file` varchar(100) NOT NULL,
  `lod_file` varchar(100) NOT NULL,
  `pvalthld` decimal(13,12) NOT NULL,
  `lodthld` decimal(6,3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_experiment_e1800d51` (`species_id`),
  KEY `upload_experiment_c5c1f9d1` (`upload_user_id`),
  CONSTRAINT `fk_main_experiment_species` FOREIGN KEY (`species_id`) REFERENCES `main_species` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

CREATE TABLE `main_gene_go` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gene_id` varchar(30) DEFAULT NULL,
  `term_accession` varchar(30) DEFAULT NULL,
  `term_evidence_code` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `term_accession` (`term_accession`),
  KEY `fk_main_gene_go_geneinfo_idx` (`gene_id`),
  CONSTRAINT `fk_main_gene_go_geneinfo` FOREIGN KEY (`gene_id`) REFERENCES `main_geneinfo` (`gene_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `main_gene_go_ibfk_1` FOREIGN KEY (`term_accession`) REFERENCES `main_go` (`accession`)
) ENGINE=InnoDB AUTO_INCREMENT=162310 DEFAULT CHARSET=latin1;

CREATE TABLE `main_geneinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gene_id` varchar(30) DEFAULT NULL,
  `gene_name` varchar(30) DEFAULT NULL,
  `description` text,
  `species_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transcript_name_UNIQUE` (`gene_id`),
  KEY `fk_main_geneinfo_species_idx` (`species_id`),
  CONSTRAINT `fk_main_geneinfo_species` FOREIGN KEY (`species_id`) REFERENCES `main_species` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=33604 DEFAULT CHARSET=latin1;

CREATE TABLE `main_go` (
  `accession` varchar(30) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `definition` text,
  `domain` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`accession`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `main_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `line_name` varchar(50) NOT NULL,
  `experiment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_main_line_experiment_idx` (`experiment_id`),
  CONSTRAINT `fk_main_line_experiment` FOREIGN KEY (`experiment_id`) REFERENCES `main_experiment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1339 DEFAULT CHARSET=utf8;

CREATE TABLE `main_marker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marker_name` varchar(15) NOT NULL,
  `marker_chr` varchar(10) DEFAULT NULL,
  `marker_start` int(11) DEFAULT NULL,
  `marker_end` int(11) DEFAULT NULL,
  `experiment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2366 DEFAULT CHARSET=utf8;

CREATE TABLE `main_species` (
  `species_name` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `main_transcript` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transcript_name` varchar(30) DEFAULT NULL,
  `spot_id_id` int(11) NOT NULL,
  `ref_id` varchar(20) DEFAULT NULL,
  `chr` varchar(3) NOT NULL,
  `start` int(11) DEFAULT NULL,
  `end` int(11) DEFAULT NULL,
  `description` text,
  `gene_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_transcript_948f9f70` (`spot_id_id`),
  KEY `idx_transcript_name` (`transcript_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=218422 DEFAULT CHARSET=utf8;

