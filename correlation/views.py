from os import path

from django.shortcuts import render_to_response, HttpResponse
from django.conf import settings

import suggestions
from main.views import no_results
from main.models import Experiment, GeneInfo, Species
from main.utils import load_genelist_for_experiment, load_lodscores_for_experiment

import numpy as np
from scipy import stats

# Create your views here.

class GeneInfoCorrelation:
    transcript_name = ""
    description = ""
    gene_name = ""
    chr = 0
    start = 0
    correlation = 0


def correlation(request):
    '''
    select a gene and experiment, and find the top genes that show correlation
    
    '''
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)
    species_short_name = Species.objects.get(species_name=settings.SPECIES).short_name

    if request.method == 'GET':
        if request.GET.get('query'):
            if request.GET.get('experiment_name'):
                exp_name = request.GET.get('experiment_name')
            else:
                exp_name = experiments[0].experiment_name
            gene_id = request.GET.get('query').strip()

            if not gene_id:
                return render_to_response('correlation.html', {'experiments': experiments})

            geneInfoList = GeneInfo.objects.filter(gene_id = gene_id, species__species_name = settings.SPECIES)
            if geneInfoList.count() == 0:
                geneInfoList = GeneInfo.objects.filter(gene_name__iexact=gene_id)

                if geneInfoList.count() == 0:
                    geneInfoList = GeneInfo.objects.filter(description__icontains=gene_id)

                if geneInfoList.count() != 1:
                    ret = suggestions.views.suggest(gene_id, exp_name, "correlation")
                    if ret:
                        return ret
                    else:
                        return no_results(gene_id, "correlation")

            geneInfo = geneInfoList[0]

            queryGene = dict()
            queryGene["id"] = geneInfo.gene_id
            queryGene["name"] = geneInfo.gene_name
            queryGene["chromosome"] = geneInfo.chr
            queryGene["start"] = geneInfo.start

            corrthld = 0.9
            if request.GET.get('corrthld'):
                try:
                    corrthld = float(request.GET.get('corrthld'))
                    if corrthld > 1:
                        corrthld = 1
                    elif corrthld < -1:
                        corrthld = -1
                except ValueError:
                    return HttpResponse('<h1> invalid correlation threshold </h1>')

            genes = getCorrelatingGenes(queryGene["id"].upper(), exp_name,corrthld)

            genelist = list()
            for gene in genes:
                g = GeneInfoCorrelation()
                g.gene_id = gene
                g.correlation = genes[gene]
                gi = GeneInfo.objects.filter(gene_id=gene)
                if gi:
                    g.description = gi[0].description
                    g.gene_name = gi[0].gene_name
                    g.chr = gi[0].chr
                    g.start = gi[0].start
                genelist.append(g)

            return render_to_response('correlation.html', {'experiment_name': exp_name,
                                                           'experiments': experiments,
                                                           'species': species_short_name,
                                                           'queryGene': queryGene,
                                                           'gene_list': sorted(genelist, key=lambda x: x.correlation,
                                                                               reverse=True),
                                                           'corrthld': corrthld})
        else:
            return render_to_response('correlation.html', {'experiments': experiments})


def getCorrelatingGenes(gene, exp_name, corrthld):
    lod_scores = load_lodscores_for_experiment(exp_name)
    genes = load_genelist_for_experiment(exp_name)

    ms = lod_scores.mean(axis=1)[(slice(None, None, None), None)]
    datam = lod_scores - ms
    datass = np.sqrt(stats.ss(datam, axis=1))
    gene_index = genes.tolist().index(gene)
    temp = np.dot(datam, datam[gene_index].T)
    rs = temp / (datass * datass[gene_index])

    return dict(zip(genes[rs>corrthld],rs[rs>corrthld]))