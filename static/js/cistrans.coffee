# cistrans.coffee
#
# Interactive cis-trans eQTL plot
#
# In top figure, x-axis corresponds to marker location, y-axis is
# genomic position of probes on a gene expression microarray Each
# plotted point is an inferred eQTL with LOD > 10; opacity corresponds
# to LOD score, though all LOD > 25 are made fully opaque.
#
# Hover over a point to see probe ID and LOD score; also highlighted
# are any other eQTL for that probe.  Click on the point to see LOD
# curves below.
#
# If a clicked probe is in known gene, title in lower plot is a link
# to the Mouse Genome Informatics (MGI) site at the Jackson Lab.
#
# Hover over markers in LOD curve plot to view marker names; click on
# a marker to see the phenotype-vs-genotype plot to right.  In
# geno-vs-pheno plot, hover over average to view value, and hover over
# points to view individual IDs.

selectedGene = ""

# function that does all of the work
draw = (data) ->

  d3.select("div#loading").remove()

  totalw = 1100
  totalh = 1100

  # dimensions of panels
  h = [499, 200, 100, 170,170]
  w = [500, 440, 500, 440, 440]
  pad = {left:60, top:40, right:40, bottom: 40, inner: 10}

  left = [60, 650, 60, 650, 650]
  right = [560, 1090, 560, 1090, 1090]
  top =  [41, 41, 570, 300, 500]
  bottom = [540, 241, 670, 470, 670]

  # Size of rectangles in top-left panel
  peakRad = 2
  bigRad = 5

  # gap between chromosomes in lower plot
  chrGap = 8

  # transition speeds
  slowtime = 1000
  fasttime = 250

  # height of marker ticks in lower panel
  tickHeight = (bottom[1] - top[1])*0.02
  jitterAmount = (right[3] - left[3])/50
  jitter = []
  for i of data.individuals
    jitter[i] = (2.0*Math.random()-1.0) * jitterAmount

  nodig = d3.format(".0f")
  onedig = d3.format(".1f")
  twodig = d3.format(".2f")

  # colors definitions
  lightGray = d3.rgb(230, 230, 230)
  darkGray = d3.rgb(200, 200, 200)
  darkblue = "darkslateblue"
  darkgreen = "darkslateblue" # "darkgreen" # <- I didn't like having separate colors for each chr in cis/trans plot
  pink = "hotpink"
  altpink = "#E9CFEC"
  purple = "#8C4374"
  darkred = "crimson"
  # bgcolor = "black"
  labelcolor = "black"   # "white"
  titlecolor = "blue"    # "Wheat"
  maincolor = "darkblue" # "Wheat"

  # calculate X and Y scales, using bp positions
  totalChrLength = 0;
  for c in data.chrnames
    data.chr[c].length = data.chr[c].end - data.chr[c].start
    totalChrLength += data.chr[c].length

  chrXScale = {}
  chrYScale = {}
  curXPixel = left[0]+peakRad
  curYPixel = bottom[0]-peakRad
  for c in data.chrnames
    data.chr[c].length_pixel = Math.round((w[0]-peakRad*2) * data.chr[c].length / totalChrLength)
    data.chr[c].start_Xpixel = curXPixel
    data.chr[c].end_Xpixel = curXPixel + data.chr[c].length_pixel - 1
    data.chr[c].start_Ypixel = curYPixel
    data.chr[c].end_Ypixel = curYPixel - (data.chr[c].length_pixel - 1)

    chrXScale[c] = d3.scale.linear()
                  .domain([data.chr[c].start, data.chr[c].end])
                  .range([data.chr[c].start_Xpixel, data.chr[c].end_Xpixel])
                  .clamp(true)
    chrYScale[c] = d3.scale.linear()
                  .domain([data.chr[c].start, data.chr[c].end])
                  .range([data.chr[c].start_Ypixel, data.chr[c].end_Ypixel])
                  .clamp(true)

    curXPixel += data.chr[c].length_pixel
    curYPixel -= data.chr[c].length_pixel

  # slight adjustments
  top[0] = data.chr["X"].end_Ypixel-peakRad
  h[0] = bottom[0] - top[0]
  data.chr["I"].start_Xpixel = left[0]
  data.chr["I"].start_Ypixel = bottom[0]
  data.chr["mt"].end_Xpixel = right[0]
  data.chr["mt"].end_Ypixel = top[0]

  # chr scales in lower figure
  chrLowXScale = {}
  cur = Math.round(left[1] + chrGap/2)
  for c in data.chrnames
    data.chr[c].start_lowerXpixel = cur
    data.chr[c].end_lowerXpixel = cur + Math.round((w[1]-chrGap*(data.chrnames.length))/totalChrLength*data.chr[c].length)
    chrLowXScale[c] = d3.scale.linear()
                        .domain([data.chr[c].start, data.chr[c].end])
                        .range([data.chr[c].start_lowerXpixel, data.chr[c].end_lowerXpixel])
    cur = data.chr[c].end_lowerXpixel + chrGap

  # X scales for PXG plot
  # autosome in intercross: 6 cases
  pxgXscaleA = d3.scale.ordinal()
                 .domain(d3.range(2))
                 .rangePoints([left[3], right[3]], 1)

  # create SVG
  svg = d3.select("div#cistrans").append("svg")
          .attr("width", totalw)
          .attr("height", totalh)

  # gray backgrounds
  for j of left
    svg.append("rect")
       .attr("x", left[j])
       .attr("y", top[j])
       .attr("height", h[j])
       .attr("width", w[j])
       .attr("class", "innerBox")

  # add dark gray rectangles to define chromosome boundaries as checkerboard
  checkerboard = svg.append("g").attr("id", "checkerboard")
  for ci,i in data.chrnames
    for cj,j in data.chrnames
      if((i + j) % 2 == 0)
        checkerboard.append("rect")
           .attr("x", data.chr[ci].start_Xpixel)
           .attr("width", data.chr[ci].end_Xpixel - data.chr[ci].start_Xpixel)
           .attr("y", data.chr[cj].end_Ypixel)
           .attr("height", data.chr[cj].start_Ypixel - data.chr[cj].end_Ypixel)
           .attr("stroke", "none")
           .attr("fill", darkGray)
           .style("pointer-events", "none")

  # same in lower panel
  checkerboard2 = svg.append("g").attr("id", "checkerboard2")
  for ci,i in data.chrnames
      if(i % 2 == 0)
        checkerboard2.append("rect")
           .attr("x", data.chr[ci].start_lowerXpixel - chrGap/2)
           .attr("width", data.chr[ci].end_lowerXpixel - data.chr[ci].start_lowerXpixel + chrGap)
           .attr("y", top[1])
           .attr("height", h[1])
           .attr("stroke", "none")
           .attr("fill", darkGray)
           .style("pointer-events", "none")

  # chromosome labels
  axislabels = svg.append("g").attr("id", "axislabels").style("pointer-events", "none")
  axislabels.append("g").attr("id", "topleftX").selectAll("empty")
     .data(data.chrnames)
     .enter()
     .append("text")
     .text((d) -> d)
     .attr("x", (d) -> (data.chr[d].start_Xpixel + data.chr[d].end_Xpixel)/2)
     .attr("y", bottom[0] + pad.bottom*0.3)
     .attr("fill", labelcolor)
  axislabels.append("g").attr("id", "topleftY")
     .selectAll("empty")
     .data(data.chrnames)
     .enter()
     .append("text")
     .text((d) -> d)
     .attr("x", left[0] - pad.left*0.15)
     .attr("y", (d) -> (data.chr[d].start_Ypixel + data.chr[d].end_Ypixel)/2)
     .style("text-anchor", "end")
     .attr("fill", labelcolor)
  axislabels.append("g").attr("id", "bottomX").selectAll("empty")
     .data(data.chrnames)
     .enter()
     .append("text")
     .text((d) -> d)
     .attr("x", (d) -> (data.chr[d].start_lowerXpixel + data.chr[d].end_lowerXpixel)/2)
     .attr("y", bottom[1] + pad.bottom*0.3)
     .attr("fill", labelcolor)
  axislabels.append("text").text("Cis-trans plot")
      .attr("x", (left[0] + right[0]) / 2)
      .attr("y", top[1] - pad.bottom * 0.75)
      .attr("fill", titlecolor)
      .attr("text-anchor", "middle")
      .attr("font-weight","bold")
  axislabels.append("text").text("click on a dot to see the corresponding eQTL pattern")
      .attr("x", (left[0] + right[0]) / 2).attr("y", top[1] - pad.bottom * 0.25)
      .attr("fill", titlecolor)
      .attr("text-anchor", "middle")
      .attr("font-style", "italic")
  axislabels.append("text")
     .text("Marker position (bp)")
     .attr("x", (left[0] + right[0])/2)
     .attr("y", bottom[2] + pad.bottom*0.75)
     .attr("fill", titlecolor)
     .attr("text-anchor", "middle")
  axislabels.append("text")
     .text("Marker position (bp)")
     .attr("x", (left[1] + right[1])/2)
     .attr("y", bottom[1] + pad.bottom*0.75)
     .attr("fill", titlecolor)
     .attr("text-anchor", "middle")
  xloc = left[0] - pad.left*0.65
  yloc = (top[0] + bottom[0])/2
  axislabels.append("text")
     .text("Gene position (bp)")
     .attr("x", xloc)
     .attr("y", yloc)
     .attr("transform", "rotate(270,#{xloc},#{yloc})")
     .style("text-anchor", "middle")
     .attr("fill", titlecolor)
  xloc = left[1] - pad.left*0.65
  yloc = (top[1] + bottom[1])/2
  axislabels.append("text")
     .text("LOD score")
     .attr("x", xloc)
     .attr("y", yloc)
     .attr("transform", "rotate(270,#{xloc},#{yloc})")
     .style("text-anchor", "middle")
     .attr("fill", titlecolor)

  # overall maximum lod score
  maxlod = d3.max(data.peaks, (d) -> d.lod)

  # sort peaks to have increasing LOD score
  data.peaks = data.peaks.sort (a,b) ->
    return if a.lod < b.lod then -1 else +1

  # LOD score controls opacity
  Zscale = d3.scale.linear()
             .domain([-100, 100])
             .range([0, 1])

  # tool tips using https://github.com/Caged/d3-tip
  #   [slightly modified in https://github.com/kbroman/d3-tip]
  eqtltip = d3.tip()
                 .attr("class", "d3-tip")
                 .offset([-10,0])
                 .html((z) -> "Gene: " + z.spot + "<br>Marker: " + z.marker + "<br>LOD: " + onedig(z.lod))
  markercounttip = d3.tip()
                 .attr("class", "d3-tip")
                 .offset([-10,0])
                 .html((z) -> "Marker: " + z + "<br>Peaks: " + markerSums[z])
  martip = d3.tip()
                 .attr("class", "d3-tip")
                 .offset([-10,0])
                 .html((d) -> "Marker: " + d[0] + "<br>LOD score: " + onedig(d[1]))
  efftip = d3.tip()
                 .attr("class", "d3-tip")
                 .offset([-10,0])
                 .html((d) -> twodig(d))
  indtip = d3.tip()
                .attr("class", "d3-tip")
                .offset([-10,0])
                .html((i) -> "Line: " + data.individuals[i])


  # create indices to lod scores, split by chromosome
  cur = 0
  for c in data.chrnames
    for p in data.pmarknames[c]
      data.pmark[p].index = cur
      cur++

  # function for drawing lod curve for probe
  draw_probe = (probe_data) ->
    # delete all related stuff
    svg.selectAll(".probe_data").remove()
    svg.selectAll(".plotPXG").remove()

    # find marker with maximum LOD score
    maxlod = -1
    maxlod_marker = null
    minlod = 1
    minlod_marker = null

    for m in data.markers
      lod = Math.abs(probe_data.lod[data.pmark[m].index])
      if maxlod < lod
        maxlod = lod
        maxlod_marker = m

    # y-axis scale
    lodcurve_yScale = d3.scale.linear()
                        .domain([0, maxlod*1.05])
                        .range([bottom[1], top[1]])

    # y-axis
    yaxis = svg.append("g").attr("class", "probe_data").attr("id", "loweryaxis")
    ticks = lodcurve_yScale.ticks(6)
    yaxis.selectAll("empty")
         .data(ticks)
         .enter()
         .append("line")
         .attr("y1", (d) -> lodcurve_yScale(d))
         .attr("y2", (d) -> lodcurve_yScale(d))
         .attr("x1", left[1])
         .attr("x2", right[1])
         .attr("stroke", "white")
         .attr("stroke-width", "1")
    yaxis.selectAll("empty")
         .data(ticks)
         .enter()
         .append("text")
         .text((d) ->
            return if maxlod > 10 then nodig(d) else onedig(d))
         .attr("y", (d) -> lodcurve_yScale(d))
         .attr("x", left[1] - pad.left*0.1)
         .style("text-anchor", "end")
         .attr("fill", labelcolor)

    # lod curves by chr
    lodcurve = (c) ->
        d3.svg.line()
          .x((p) -> chrLowXScale[c](data.pmark[p].start))
          .y((p) -> lodcurve_yScale(Math.abs(probe_data.lod[data.pmark[p].index])))
    curves = svg.append("g").attr("id", "curves").attr("class", "probe_data")
    for c in data.chrnames
      curves.append("path")
            .datum(data.pmarknames[c])
            .attr("d", lodcurve(c))
            .attr("class", "thickline")
            .attr("stroke", darkblue)
            .style("pointer-events", "none")
            .attr("fill", "none")

    # title
    titletext = probe_data.probe
    probeaxes = svg.append("g").attr("id", "probe_data_axes").attr("class", "probe_data")
    gene = data.spot[probe_data.probe.toUpperCase()].gene_name
    gene_url = "/AraQTL/multiplot/?query=#{probe_data.probe}"
    if gene isnt null
      titletext += " (#{gene})"
      xlink = probeaxes.append("a").attr("xlink:href", gene_url)
      xlink.append("text")
         .text(titletext)
         .attr("x", (left[1]+right[1])/2)
         .attr("y", top[1] - pad.top/2)
         .attr("fill", titlecolor)
         .style("font-size", "18px")
    else
      probeaxes.append("text")
         .text(titletext)
         .attr("x", (left[1]+right[1])/2)
         .attr("y", top[1] - pad.top/2)
         .attr("fill", titlecolor)
         .style("font-size", "18px")

    # black border
    svg.append("rect").attr("class", "probe_data")
       .attr("x", left[1])
       .attr("y", top[1])
       .attr("height", h[1])
       .attr("width", w[1])
       .attr("class", "outerBox")

    # point at probe
    svg.append("circle")
       .attr("class", "probe_data")
       .attr("id", "probe_circle")
       .attr("cx", chrLowXScale[data.spot[probe_data.probe.toUpperCase()].chr](data.spot[probe_data.probe.toUpperCase()].start))
       .attr("cy", top[1])
       .attr("r", bigRad)
       .attr("fill", pink)
       .attr("stroke", darkblue)
       .attr("stroke-width", 1)
       .attr("opacity", 1)

    # keep track of clicked marker
    markerClick = {}
    for m in data.markers
      markerClick[m] = 0
    lastMarker = ""

    # dots at markers on LOD curves
    svg.call(martip)
    svg.append("g").attr("id", "markerCircle").attr("class", "probe_data")
       .selectAll("empty")
       .data(data.markers)
       .enter()
       .append("circle")
       .attr("class", "probe_data")
       .attr("id", (td) -> "marker_{td}")
       .attr("cx", (td) -> chrLowXScale[data.pmark[td].chr](data.pmark[td].start))
       .attr("cy", (td) -> lodcurve_yScale(probe_data.lod[data.pmark[td].index]))
       .attr("r", bigRad)
       .attr("fill", purple)
       .attr("stroke", "none")
       .attr("stroke-width", "2")
       .attr("opacity", 0)
       .on("mouseover", (td) ->
              d3.select(this).attr("opacity", 1) unless markerClick[td]
              martip.show([td, probe_data.lod[data.pmark[td].index]]))
       .on "mouseout", (td) ->
              d3.select(this).attr("opacity", markerClick[td])
              martip.hide() 
       .on "click", (td) ->
              pos = data.pmark[td].start
              chr = data.pmark[td].chr
              plotPXG td
              lastMarker = td
              markerClick[td] = 1
              d3.select(this).attr("opacity", 1).attr("fill",altpink).attr("stroke",purple)

    draw_pxgXaxis = (means, genotypes, chr) ->
      pxgXaxis.selectAll("line.PXGvert")
              .data(means)
              .enter()
              .append("line")
              .attr("class", "PXGvert")
              .attr("y1", top[3])
              .attr("y2", bottom[3])
              .attr("x1", (d,i) -> return pxgXscaleA(i))
              .attr("x2", (d,i) -> return pxgXscaleA(i))
              .attr("stroke", darkGray)
              .attr("fill", "none")
              .attr("stroke-width", "1")
      pxgXaxis.selectAll("line.PXGvert")
              .data(means)
              .transition().duration(fasttime)
              .attr("x1", (d,i) -> return pxgXscaleA(i))
              .attr("x2", (d,i) -> return pxgXscaleA(i))
      pxgXaxis.selectAll("line.PXGvert")
              .data(means)
              .exit().remove()
      pxgXaxis.selectAll("text.PXGgeno")
              .data(genotypes)
              .enter()
              .append("text")
              .attr("class", "PXGgeno")
              .text((d) -> d)
              .attr("y", bottom[3] + pad.bottom*0.25)
              .attr("x",  (d,i) -> return pxgXscaleA(i))
              .attr("fill", labelcolor)
      pxgXaxis.selectAll("text.PXGgeno")
              .data(genotypes)
              .transition().duration(fasttime)
              .text((d) -> d)
              .attr("x",  (d,i) -> return pxgXscaleA(i))
      pxgXaxis.selectAll("text.PXGgeno")
              .data(genotypes)
              .exit().remove()

      # add line segments
      meanmarks.selectAll("line.PXGmeans")
         .data(means)
         .enter()
         .append("line")
         .attr("class", "PXGmeans")
         .attr("x1", (d,i) ->
            return pxgXscaleA(i)-jitterAmount*3)
         .attr("x2", (d,i) ->
            return pxgXscaleA(i)+jitterAmount*3)
         .attr("y1", (d) -> pxgYscale(d))
         .attr("y2", (d) -> pxgYscale(d))
         .attr("stroke", (d,i) -> return darkred)
         .attr("stroke-width", 4)
         .on("mouseover", efftip)
         .on("mouseout", -> d3.selectAll("#efftip").remove())
      meanmarks.selectAll("line.PXGmeans")
         .data(means)
         .transition().duration(slowtime)
         .attr("x1", (d,i) ->
            return pxgXscaleA(i)-jitterAmount*3)
         .attr("x2", (d,i) ->
            return pxgXscaleA(i)+jitterAmount*3)
         .attr("y1", (d) -> pxgYscale(d))
         .attr("y2", (d) -> pxgYscale(d))
         .attr("stroke", (d,i) -> return darkred)
      meanmarks.selectAll("line.PXGmeans")
         .data(means).exit().remove()

    pxgYscale = null
    pxgXaxis = svg.append("g").attr("class", "probe_data").attr("id", "pxg_xaxis")
    pxgYaxis = svg.append("g").attr("class", "probe_data").attr("class", "plotPXG").attr("id", "pxg_yaxis")
    meanmarks = svg.append("g").attr("id", "pxgmeans").attr("class", "probe_data")

    plotPXG = (marker) ->
      if not probe_data.pheno?
        return
      pxgYscale = d3.scale.linear()
                     .domain([d3.min(probe_data.pheno),
                              d3.max(probe_data.pheno)])
                     .range([bottom[3]-pad.inner, top[3]+pad.inner])
      pxgticks = pxgYscale.ticks(8)
      pxgYaxis.selectAll("empty")
         .data(pxgticks)
         .enter()
         .append("line")
         .attr("y1", (d) -> pxgYscale(d))
         .attr("y2", (d) -> pxgYscale(d))
         .attr("x1", left[3])
         .attr("x2", right[3])
         .attr("stroke", "white")
         .attr("stroke-width", "1")
      pxgYaxis.selectAll("empty")
         .data(pxgticks)
         .enter()
         .append("text")
         .text((d) -> twodig(d))
         .attr("y", (d) -> pxgYscale(d))
         .attr("x", left[3] - pad.left*0.1)
         .style("text-anchor", "end")
         .attr("fill", labelcolor)

      # calculate group averages
      chr = data.pmark[marker].chr

      means = [0,0]
      n = [0,0]

      genotypes = ["CB4856", "N2"]

      for i of data.individuals
         g = data.geno[marker][i]
         x = (1 + g)/2
         means[x] += probe_data.pheno[i]
         n[x]++
      for i of means
        means[i] /= n[i]

      draw_pxgXaxis(means, genotypes, chr)

      svg.call(indtip)
      svg.append("g").attr("id", "plotPXG").attr("class", "probe_data").attr("id","PXGpoints").selectAll("empty")
          .data(probe_data.pheno)
          .enter()
          .append("circle")
          .attr("class", "plotPXG")
          .attr("cx", (d,i) ->
              g = data.geno[marker][i]
              cx = pxgXscaleA((1 + g)/2) + jitter[i]
              cx)
          .attr("cy", (d) -> pxgYscale(d))
          .attr("r", peakRad)
          .attr("fill", (d,i) ->
                g = data.geno[marker][i]
                if (g == -1) then return "blue" else return "orange")
          .attr("stroke", (d,i) ->
                g = data.geno[marker][i]
                if (g == -1) then return "blue" else return "orange")
          .attr("stroke-width", 2)
          .on "mouseover", (d,i) ->
               d3.select(this).attr("r", bigRad)
               indtip.show(i)
          .on "mouseout", ->
               indtip.hide()
               d3.select(this).attr("r", peakRad)


    # initially select the marker with maximum LOD
    lastMarker = maxlod_marker
    markerClick[lastMarker] = 1
    d3.select("circle[id='marker_{lastMarker}']").attr("opacity", 1).attr("fill",altpink).attr("stroke",purple)
    pos = data.pmark[lastMarker].start
    chr = data.pmark[lastMarker].chr
    plotPXG(lastMarker)

  chrindex = {}
  for c,i in data.chrnames
    chrindex[c] = i

  markerSums = {}
  for m in data.peaks
    if (!(m.marker of markerSums)) then markerSums[m.marker] = 0
    markerSums[m.marker]++

  maxMarkerSum =0
  for m in data.markers
    if (markerSums[m] > maxMarkerSum) then maxMarkerSum = markerSums[m]

  histylabels = svg.append("g").attr("id", "histylabels").style("pointer-events", "none")
  histylabels.append("text").text(maxMarkerSum).attr("x", left[2])
            .attr("y", top[2])
            .style("text-anchor", "end")
            .attr("fill", labelcolor)

  histylabels.append("text").text("0").attr("x", left[2])
            .attr("y", bottom[2])
            .style("text-anchor", "end")
            .attr("fill", labelcolor)

  xloc = left[2] - pad.left * 0.65
  yloc = (top[2] + bottom[2]) / 2
  histylabels.append("text")
            .text("#eQTLs")
            .attr("x", xloc)
            .attr("y", yloc)
            .attr("transform", "rotate(270," + xloc + "," + yloc + ")")
            .style("text-anchor", "middle")
            .attr("fill", titlecolor);

  svg.call(markercounttip)
  markers = svg.append("g").attr("id", "histpeaks").selectAll("empty").data(data.markers).enter().append("line")
     .attr("class", (d) -> return "marker_" + d)
     .attr("x1", (d) -> return chrXScale[data.pmark[d].chr](data.pmark[d].start))
     .attr("x2", (d) -> return chrXScale[data.pmark[d].chr](data.pmark[d].start))
     .attr("y1", (d) -> 
        if d of markerSums  
            return bottom[2] - h[2] * (markerSums[d]/maxMarkerSum) + 2
        else
            return bottom[2]
	)
     .attr("y2", (d) -> return bottom[2])
     .attr("stroke-width", 3)
     .attr("stroke", darkblue).on("mouseover", (d) -> return markercounttip.show(d))
     .on("mouseout", (d) -> return markercounttip.hide())
     .on("click", (d) -> location.href="/AraQTL/coregulation/?experiment_name={{ experiment_name }}&query="+d+"&thld={{ lodthld }}")

  # circles at eQTL peaks
  svg.call(eqtltip)
  peaks = svg.append("g").attr("id", "peaks")
             .selectAll("empty")
             .data(data.peaks)
             .enter()
             .append("circle")
             .attr("class", (d) -> "probe_#{d.spot}")
             .attr("cx", (d) -> chrXScale[data.pmark[d.marker].chr](data.pmark[d.marker].start))
             .attr("cy", (d) -> chrYScale[data.spot[d.spot].chr](data.spot[d.spot].start))
             .attr("r", peakRad)
             .attr("stroke", "none")
             .attr("fill", (d) -> return if(d.lod > 0) then darkblue else darkred)
             .attr("opacity", (d) -> Zscale(d.lod))
             .on "mouseover", (d) ->
                 d3.selectAll("circle.probe_#{d.spot}")
                                .attr("r", bigRad)
                                .attr("fill", pink)
                                .attr("stroke", darkblue)
                                .attr("stroke-width", 1)
                                .attr("opacity", 1)
                 eqtltip.show(d)
             .on "mouseout", (d) ->
                 d3.selectAll("circle.probe_#{d.spot}")
                                .attr("r", peakRad)
                                .attr("fill", (d) -> return if(chrindex[d.chr] % 2 is 0) then darkblue else darkgreen)
                                .attr("stroke", "none")
                                .attr("opacity", (d) -> Zscale(d.lod))
                 eqtltip.hide()
             .on "click", (d) ->
                 d3.json("../media/data/" + experiment_name + "/probe/" + d.spot + ".json", draw_probe)

  experimentinfo = svg.append("g").attr("id", "experimentinfo").style("pointer-events", "none")


  lines = [
    "Experiment: " + data.experiment_name,
    "Cross: " + data.parental_strain,
    "Sample: " +data.sample,
    "Publication: " + data.reference,
    "LOD score threshold: " + data.lodthld
    ]

  iLine = 0
  offset = 20
  for line in lines
    experimentinfo.append("text").text(line).attr("x", left[4]+10)
        .attr("y", top[4] + offset + 30*iLine)
        .style("text-anchor", "start")
        .attr("fill", maincolor)
    iLine++

  # black borders
  for j of left
    svg.append("rect")
       .attr("x", left[j])
       .attr("y", top[j])
       .attr("height", h[j])
       .attr("width", w[j])
       .attr("class", "outerBox")

  # autocomplete
  $('input#genesymbol').autocomplete({
    autoFocus: true,
    source: (request, response) ->
      matches = $.map(allgenes, (tag) ->
        tag if tag.toUpperCase().indexOf(request.term.toUpperCase()) is 0)
      response(matches)
    ,
    select: (event, ui) ->
      $('input#genesymbol').val(ui.item.label)
      $('input#submit').submit()})

# grayed-out "Gene symbol" within text input box
$('input#genesymbol').each(() ->
  $(this)
    .data('default', $(this).val())
    .addClass('inactive')
    .focus(() ->
      $(this).removeClass('inactive')
      $(this).val('') if($(this).val() is $(this).data('default') or $(this).val() is '')
    )
    .blur(() ->
      if($(this).val() is '')
        $(this).addClass('inactive').val($(this).data('default'))
    )
  )

# load json file and call draw function
draw_cisqtl_plot = () -> d3.json("../media/data/" + experiment_name + "/lod" + thld_ + ".json", draw)
