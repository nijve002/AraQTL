# multiplot.coffee
#
# Interactive eQTL plot
#
# adapted from https://github.com/kbroman/d3examples/tree/master/cistrans
#

# function that does all of the work
draw = (data) ->
  d3.select("div#loading").remove()
  d3.select("div#legend").style("opacity", 1)
  d3.select("div#geneinput").style("opacity", 1)

  # dimensions of panels
  w = 700
  h = 300
  pad = {left:60, top:40, right:40, bottom: 40, inner: 10}

  left = 60
  right = 760
  top =  41
  bottom = top + h
  leftLegend = right + 10
  topLegend = top
  hLegend = h
  wLegend = 290

  totalw = 1100
  totalh = 400

  peakRad = 2
  bigRad = 5

  maxLegendLines = 15

  # gap between chromosomes
  chrGap = 8

  # height of marker ticks
  tickHeight = (bottom - top)*0.02

  nodig = d3.format(".0f")
  onedig = d3.format(".1f")
  twodig = d3.format(".2f")

  # colors definitions
  darkGray = d3.rgb(200, 200, 200)
  pink = "hotpink"
  darkblue = "darkslateblue"
  purple = "#8C4374"
  labelcolor = "black"   # "white"
  titlecolor = "blue"    # "Wheat"
  maincolor = "darkblue" # "Wheat"

  # calculate X and Y scales, using bp positions
  totalChrLength = 0
  for c in data.chrnames
    data.chr[c].length_bp = data.chr[c].end - data.chr[c].start
    totalChrLength += data.chr[c].length_bp

  curXPixel = left+peakRad
  curYPixel = bottom-peakRad
  for c in data.chrnames
    data.chr[c].length_pixel = Math.round((w-peakRad*2) * data.chr[c].length_bp / totalChrLength)
    data.chr[c].start_Xpixel = curXPixel
    data.chr[c].end_Xpixel = curXPixel + data.chr[c].length_pixel - 1
    data.chr[c].start_Ypixel = curYPixel
    data.chr[c].end_Ypixel = curYPixel - (data.chr[c].length_pixel - 1)

    curXPixel += data.chr[c].length_pixel
    curYPixel -= data.chr[c].length_pixel

  # slight adjustments
  data.chr["I"].start_Xpixel = left
  data.chr["I"].start_Ypixel = bottom

  # chr scales 
  chrXScale = {}
  cur = Math.round(pad.left + chrGap/2)
  for c in data.chrnames
    data.chr[c].start_Xpixel = cur
    data.chr[c].end_Xpixel = cur + Math.round((w-chrGap*(data.chrnames.length))/totalChrLength*data.chr[c].length_bp)
    chrXScale[c] = d3.scale.linear()
                        .domain([data.chr[c].start, data.chr[c].end])
                        .range([data.chr[c].start_Xpixel, data.chr[c].end_Xpixel])
    cur = data.chr[c].end_Xpixel + chrGap

  # create SVG
  d3.select("svg").remove()
  svg = d3.select("div#multiplot").append("svg")
          .attr("width", totalw)
          .attr("height", totalh)

  # gray backgrounds
  svg.append("rect")
       .attr("x", left)
       .attr("y", top)
       .attr("height", h)
       .attr("width", w)
       .attr("class", "innerBox")

  svg.append("rect")
       .attr("x", leftLegend)
       .attr("y", topLegend)
       .attr("height", hLegend)
       .attr("width", wLegend)
       .attr("class", "innerBox")

  # add dark gray rectangles to define chromosome boundaries as checkerboard
  checkerboard = svg.append("g").attr("id", "checkerboard")
  for ci,i in data.chrnames
      if(i % 2 == 0)
        checkerboard.append("rect")
           .attr("x", data.chr[ci].start_Xpixel - chrGap/2)
           .attr("width", data.chr[ci].end_Xpixel - data.chr[ci].start_Xpixel + chrGap)
           .attr("y", top)
           .attr("height", h)
           .attr("stroke", "none")
           .attr("fill", darkGray)
           .style("pointer-events", "none")

  axislabels = svg.append("g").attr("id", "axislabels").style("pointer-events", "none")
  axislabels.append("g").attr("id", "bottomX").selectAll("empty")
     .data(data.chrnames)
     .enter()
     .append("text")
     .text((d) -> d)
     .attr("x", (d) -> (data.chr[d].start_Xpixel + data.chr[d].end_Xpixel)/2)
     .attr("y", bottom + pad.bottom*0.3)
     .attr("fill", labelcolor)
  axislabels.append("text")
     .text("Position (bp)")
     .attr("x", (left + right)/2)
     .attr("y", bottom + pad.bottom*0.75)
     .attr("fill", titlecolor)
     .attr("text-anchor", "middle")
  xloc = left - pad.left*0.65
  yloc = (top + bottom)/2
  axislabels.append("text")
     .text("LOD score")
     .attr("x", xloc)
     .attr("y", yloc)
     .attr("transform", "rotate(270,#{xloc},#{yloc})")
     .style("text-anchor", "middle")
     .attr("fill", titlecolor)

  legend = svg.append("g").attr("id", "legend")

  # function for drawing lod curve for probe
  draw_probe = (error, probe_data_set) ->
    if error
      console.log(error)
      throw(error)

    martip = d3.tip()
             .attr("class", "d3-tip")
         .offset([-10, 0])
             .html((d) ->  "marker: " + d[0] + " (" + d[3] + ")<br>position: " + d[1] + 
				"<br>LOD score: " + d[2] + "<br><i>click for genes peaking here</i>")

    legendtip = d3.tip()
             .attr("class", "d3-tip")
         .offset([-10, 150])
             .html((d) ->  d)

    # find marker with maximum LOD score
    maxlod = -1
    for pd in probe_data_set
       for marker in Object.keys(pd.lodscores)
          lod = pd.lodscores[marker].lod 
          if maxlod < Math.abs(lod)
             maxlod = Math.abs(lod)

    # y-axis scale
    lodcurve_yScale = d3.scale.linear()
                        .domain([0, maxlod*1.05])
                        .range([bottom, top])

    # y-axis
    yaxis = svg.append("g").attr("class", "probe_data").attr("id", "yaxis")
    ticks = lodcurve_yScale.ticks(6)
    yaxis.selectAll("empty")
         .data(ticks)
         .enter()
         .append("line")
         .attr("y1", (d) -> lodcurve_yScale(d))
         .attr("y2", (d) -> lodcurve_yScale(d))
         .attr("x1", left)
         .attr("x2", right)
         .attr("stroke", "white")
         .attr("stroke-width", "1")
    yaxis.selectAll("empty")
         .data(ticks)
         .enter()
         .append("text")
         .text((d) ->
            return if maxlod > 10 then nodig(d) else onedig(d))
         .attr("y", (d) -> lodcurve_yScale(d))
         .attr("x", left - pad.left*0.1)
         .style("text-anchor", "end")
         .attr("fill", labelcolor)

    # lod curves by chr
    lineColors = d3.scale.category10()

    lodcurve = (c,ls) ->
        d3.svg.line()
          .x((p) -> chrXScale[c](ls[p].start))
          .y((p) -> lodcurve_yScale(Math.abs(ls[p].lod)))
    curves = svg.append("g").attr("id", "curves").attr("class", "probe_data")
    for probe_data, i in probe_data_set
      for c in data.chrnames
        markers_for_chromosome = do(c)->m for m,ls of probe_data.lodscores when ls.chr is c
        markers_for_chromosome.sort((a,b)-> probe_data.lodscores[a].start - probe_data.lodscores[b].start)
        curves.append("path")
            .datum(markers_for_chromosome)
            .attr("id",probe_data.gene+"-"+probe_data.experiment)
            .attr("d", lodcurve(c,probe_data.lodscores))
            .attr("class", "thickline")
            .attr("stroke", lineColors(i))
            .style("pointer-events", "none")
            .attr("fill", "none")

    # black border
    svg.append("rect").attr("class", "probe_data")
       .attr("x", left)
       .attr("y", top)
       .attr("height", h)
       .attr("width", w)
       .attr("class", "outerBox")

    if (dot?) 
      dottip = d3.tip()
         .attr("class", "d3-tip").offset([-10, 0])
         .html((d) ->  dot.label + " (" + dot.chr + ":" + dot.pos + ")")

      svg.call(dottip)
      svg.append("circle").attr("class", "probe_data")
         .attr("id", "circle")
         .attr("cx", chrXScale[dot.chr](dot.pos))
         .attr("cy", top)
         .attr("r", bigRad)
         .attr("fill", pink)
         .attr("stroke", darkblue)
         .attr("stroke-width", 1)
         .attr("opacity", 1)
         .on "mouseover", () -> dottip.show()
         .on "mouseout", () -> dottip.hide()
    else
      for probe_data,i in probe_data_set
        do(probe_data)->
          svg.append("circle")
             .attr("id", "gene_#{probe_data.gene}")
             .attr("cx", chrXScale[probe_data.chr](probe_data.start))
             .attr("cy", top)
             .attr("r", bigRad)
             .attr("fill",  lineColors(i))
             .attr("stroke", darkblue)
             .attr("stroke-width", 1)
             .attr("opacity", 1)

    svg.call(martip)

    # dots at markers on LOD curves
    for probe_data in probe_data_set
      do(probe_data)->
        svg.append("g").attr("id", "markerCircle").attr("class", "probe_data")
         .selectAll("empty")
         .data(d3.entries(probe_data.lodscores))
         .enter()
         .append("circle")
         .attr("class", "probe_data")
         .attr("id", (td) -> "marker_#{td.key}")
         .attr("cx", (td) -> chrXScale[td.value.chr](td.value.start))
         .attr("cy", (td) -> lodcurve_yScale(Math.abs(td.value.lod)))
         .attr("r", bigRad)
         .attr("fill", purple)
         .attr("stroke", "none")
         .attr("stroke-width", "2")
         .attr("opacity", 0)
         .on "mouseover", (td) ->
              d3.select(this).attr("opacity", 1) 
              if multiexperiment
                  label = probe_data.experiment
              else
                  label = probe_data.gene

              martip.show([td.key,td.value.chr+":"+td.value.start,Math.round(td.value.lod*100)/100,label])
          .on "mouseout", ((td) ->
              d3.select(this).attr("opacity", 0)
              martip.hide())
           .on "click", (td) -> 
              location.href="/AraQTL/coregulation/?experiment_name="+probe_data.experiment+"&query="+td.key


    probeaxes = svg.append("g").attr("id", "probe_data_axes").attr("class", "probe_data")
    probeaxes.append("text").text(titletxt)
      .attr("x", (left + right) / 2)
      .attr("y", top - pad.top / 2)
      .attr("fill", maincolor).style("font-size", "18px")

    svg.call(legendtip)

    for probe_data,i  in probe_data_set
      if i < maxLegendLines
        legend.append("line")
          .attr("x1",leftLegend + 10)
          .attr("y1",topLegend + 18*i + 15)
          .attr("x2",leftLegend + 40)
          .attr("y2",topLegend + 18*i + 15)
          .attr("class", "thickline")
          .attr("stroke", lineColors(i))

      linkUrl = ""
      legendtiptext = ""
      if i == maxLegendLines
        label = "..."
      else if multiexperiment
        label = probe_data.experiment
        linkUrl = "/AraQTL/cistrans/?experiment_name="+label
        legendtiptext = "<i>click for experiment details</i>"
      else if probe_data.name
        label = probe_data.gene + " (" + probe_data.name + ")" 
        linkUrl = data.url+probe_data.gene
        legendtiptext = "<i>click for gene information<br>at wormbase</i>"
      else
        label = probe_data.gene 
        linkUrl = data.url+probe_data.gene
        legendtiptext = "<i>click for gene information<br>at wormbase</i>"

      xlink = probeaxes.append("a").attr("xlink:href", linkUrl).attr("xlink:show","new")
      do(probe_data,i)->
          xlink.append("text")
            .text(label)
            .attr("x", leftLegend + 50)
            .attr("y", topLegend + 18*i + 15)
            .attr("fill", titlecolor)
            .style("text-anchor", "start")
            .on "mouseover", () ->
                svg.selectAll('path').attr("opacity", 0.1)
                svg.selectAll('path#'+probe_data.gene+'-'+probe_data.experiment).attr("opacity", 1)
                legendtip.show(legendtiptext) if legendtiptext
            .on "mouseout", () ->
                svg.selectAll('path').attr("opacity", 1)
                legendtip.hide()

      if i == maxLegendLines
        break


  # black borders
    svg.append("rect")
       .attr("x", left)
       .attr("y", top)
       .attr("height", h)
       .attr("width", w)
       .attr("class", "outerBox")

    svg.append("rect")
       .attr("x", leftLegend)
       .attr("y", topLegend)
       .attr("height", hLegend)
       .attr("width", wLegend)
       .attr("class", "outerBox")

  $("body").css("cursor", "progress");
  q = queue()
  for g,gene of genes
    q.defer(d3.json,"../media/data/"+gene.experiment+"/genes/"+gene.gene.toUpperCase()+".json") if gene.show
  q.awaitAll(draw_probe)
  $("body").css("cursor", "default");

# load json file and call draw function

draw_qtl_plot = () -> d3.json("../media/species/"+species+".json", draw)
