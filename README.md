QTL analysis platform 

Citation:
Nijveen, H., Ligterink, W., Keurentjes, J. J.B., Loudet, O., Long, J., Sterken, 
M. G., Prins, P., Hilhorst, H. W., de Ridder, D., Kammenga, J. E. and Snoek, B. 
L. (2016), AraQTL - Workbench and Archive for systems genetics in Arabidopsis 
thaliana. Plant J. Accepted Author Manuscript. doi:10.1111/tpj.13457

http://onlinelibrary.wiley.com/doi/10.1111/tpj.13457/full