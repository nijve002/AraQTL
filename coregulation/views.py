import os
import re

from django.shortcuts import render_to_response, HttpResponse
from django.conf import settings

import main
import suggestions
from main.models import Experiment, GeneInfo, Species, Marker
from main import utils

import numpy as np

# Create your views here.

class GeneInfoMarker:
    transcript_name = ""
    description = ""
    gene_name = ""
    LOD = 0
    chr = 0
    start = 0
    end = 0

def selectMarker(request):
    '''
    select a marker, experiment and threshold
    
    '''
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)
    species_short_name = Species.objects.get(species_name=settings.SPECIES).short_name

    if request.method == 'GET':
        if request.GET.get('experiment_name') and request.GET.get('query'):
            exp_name = request.GET.get('experiment_name')
            query = request.GET.get('query').strip()

            if not query:
                return render_to_response('peakmarker.html', {'experiments': experiments})

            lodthld = Experiment.objects.get(experiment_name=exp_name).lodthld

            if request.GET.get('thld'):
                try:
                    lodthld = float(request.GET.get('thld'))
                except ValueError:
                    return HttpResponse('<h1> invalid LOD threshold </h1>')

            min_dist = 0

            if request.GET.get('min_dist'):
                try:
                    min_dist = int(request.GET.get('min_dist'))
                except ValueError:
                    min_dist = 0

            marker_name = None

            if Marker.objects.filter(name=query, experiment__experiment_name=exp_name):
                marker_name = Marker.objects.filter(name=query)[0].name
            elif Marker.objects.filter(name__iexact=query, experiment__experiment_name=exp_name):
                markers = Marker.objects.filter(name__iexact=query)
                marker_name = markers[0].name
            elif Marker.objects.filter(name__iexact=query):
                markers = Marker.objects.filter(name__iexact=query)
                marker = markers[0]
                chromosome = marker.chromosome.name
                position = marker.start
                closestMarker = utils.getClosestMarker(exp_name, chromosome, position)
                if closestMarker:
                    marker_name = closestMarker.name
            elif re.match("(chr)?(\w+):(\d+)$", query,re.I):
                genomicLocation = re.match("(chr)?(\w+):(\d+)$", query,re.I)
                chromosome = genomicLocation.group(2)
                position = genomicLocation.group(3)
                closestMarker = utils.getClosestMarker(exp_name, chromosome, position)
                if closestMarker:
                    marker_name = closestMarker.name
            elif GeneInfo.objects.filter(gene_id = query):
                geneInfo = GeneInfo.objects.get(gene_id = query)
                chromosome = geneInfo.chr
                position = geneInfo.start
                closestMarker = utils.getClosestMarker(exp_name, chromosome, position)
                if closestMarker:
                    marker_name = closestMarker.name

            if marker_name == None:
                ret = suggestions.views.suggest(query, exp_name, "coregulation")
                if ret:
                    return ret
                else:
                    return main.views.no_results(query, "coregulation")

            lodscores = getLodScoresForMarker(exp_name, marker_name)
            genes = {g: l for (g, l) in lodscores.items() if l >= lodthld}

            marker = Marker.objects.get(name=marker_name,experiment__experiment_name=exp_name)

            genelist = list()
            for gene in genes:
                g = GeneInfoMarker()
                g.gene_id = gene
                g.LOD = genes[gene]
                gi = GeneInfo.objects.filter(gene_id=gene)
                if gi:
                    g.description = gi[0].description
                    g.name = gi[0].gene_name
                    g.chr = gi[0].chr
                    g.start = gi[0].start
                    g.end = gi[0].end
                    if min_dist and g.start:
                        dist = abs(long(g.start) - marker.start)/1000000
                        if g.chr == marker.chromosome.name and dist < min_dist:
                            continue
                    genelist.append(g)

            return render_to_response('peakmarker.html', {'experiment_name': exp_name,
                                                          'experiments': experiments,
                                                          'species': species_short_name,
                                                          'marker': marker,
                                                          'min_dist': min_dist,
                                                          'gene_list': sorted(genelist, key=lambda x: x.LOD,
                                                                              reverse=True),
                                                          'lodthld': lodthld})

        else:
            return render_to_response('peakmarker.html', {'experiments': experiments})



def getLodScoresForMarker(experiment_name, marker):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    lod_pickle_path = os.path.join(experiment_data_path, '%s' % "lod.npy")
    genes_pickle_path = os.path.join(experiment_data_path, '%s' % "genes.npy")
    markers_pickle_path = os.path.join(experiment_data_path, '%s' % "markers.npy")

    lod_scores = np.load(lod_pickle_path)
    genes = np.load(genes_pickle_path)
    markers = np.load(markers_pickle_path)

    mi = markers.tolist().index(marker)

    marker_lodscores = dict(zip(genes,lod_scores[:,mi]))

    return marker_lodscores
