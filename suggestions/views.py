import urllib2

from django.shortcuts import render_to_response, HttpResponse
from django.conf import settings

from main.models import GeneInfo, GO, GeneGO, Experiment

class GOTerm:
    accesssion = ""
    name = ""
    definition = ""
    domain = ""
    gene_count = 0

def index(request):
    if request.method == 'GET':

        if request.GET.get('query') and request.GET.get('experiment_name'):
            query = request.GET.get('query')
            query = urllib2.unquote(query).strip()
            experiment_name = request.GET.get('experiment_name')

            return suggest(query,experiment_name, None)

    return suggest(None,None,None)

def suggest(query, experiment_name, mode):
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)

    if not query:
        return render_to_response('suggestions.html', {'experiments': experiments})
    GO_terms = list()
    genes = list()

    go_results = GO.objects.raw(
        "select * from main_go WHERE MATCH (name, definition) AGAINST ('%s' IN NATURAL LANGUAGE MODE)" % query)

#    if experiment_name != "all":
    for go in go_results:
        go_term = GOTerm()
        go_term.accession = go.accession
        go_term.name = go.name
        go_term.definition = go.definition
        go_term.domain = go.domain
        go_term.gene_count = GeneGO.objects.filter(term_accession=go_term.accession)\
            .values("geneinfo_id").distinct().count()
        if go_term.gene_count > 0:
            GO_terms.append(go_term)

    gene_results = GeneInfo.objects.raw(
        "select * from main_geneinfo WHERE MATCH (gene_name, description) AGAINST ('%s' IN NATURAL LANGUAGE MODE)" % query)

    for gene in gene_results:
        genes.append(gene)

    gene_substring_results = GeneInfo.objects.filter(gene_name__icontains = query)

    for gene in gene_substring_results:
        genes.append(gene)

    genes = list(set(genes))

    if len(genes) == 0 and len(GO_terms) == 0:
        return None

    return render_to_response('suggestions.html', {'Genes': genes,
                                                   'GOterms': sorted(GO_terms,reverse=True,
                                                                                  key=lambda x: x.gene_count),
                                                   'mode': mode,
                                                   'experiments': experiments,
                                                   'experiment_name': experiment_name})


